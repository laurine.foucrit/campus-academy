<?php

namespace App\DataFixtures;

use App\Entity\Auteur;
use App\Entity\Livre;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $auteur = new Auteur();
        $auteur->setNom("THOUEMENT");
        $auteur->setPrenom("Thibaut");
        $manager->persist($auteur);

        $livre = new Livre();
        $livre->setNom("Le livre A");
        $livre->setAuteur($auteur);
        $livre->setDatePublication(DateTime::createFromFormat("Y-m-d", "2016-12-20"));
        $livre->setPrix(12.25);
        $livre->setStock(12);
        $manager->persist($livre);

        $livre = new Livre();
        $livre->setNom("Le livre B");
        $livre->setAuteur($auteur);
        $livre->setDatePublication(DateTime::createFromFormat("Y-m-d", "2016-12-20"));
        $livre->setPrix(1.25);
        $livre->setStock(10);
        $manager->persist($livre);


        $auteur = new Auteur();
        $auteur->setNom("FOUCRIT");
        $auteur->setPrenom("Laurine");
        $manager->persist($auteur);

        $livre = new Livre();
        $livre->setNom("Le livre C");
        $livre->setAuteur($auteur);
        $livre->setDatePublication(DateTime::createFromFormat("Y-m-d", "2016-12-20"));
        $livre->setPrix(32.50);
        $livre->setStock(150);
        $manager->persist($livre);

        $livre = new Livre();
        $livre->setNom("Le livre D");
        $livre->setAuteur($auteur);
        $livre->setDatePublication(DateTime::createFromFormat("Y-m-d", "2016-12-20"));
        $livre->setPrix(8.00);
        $livre->setStock(5);
        $manager->persist($livre);

        $livre = new Livre();
        $livre->setNom("Le livre E");
        $livre->setAuteur($auteur);
        $livre->setDatePublication(DateTime::createFromFormat("Y-m-d", "2016-12-20"));
        $livre->setPrix(18.25);
        $livre->setStock(40);
        $manager->persist($livre);

        $auteur = new Auteur();
        $auteur->setNom("MAIGNE");
        $auteur->setPrenom("Eliez");
        $manager->persist($auteur);

        $livre = new Livre();
        $livre->setNom("Le livre F");
        $livre->setAuteur($auteur);
        $livre->setDatePublication(DateTime::createFromFormat("Y-m-d", "2016-12-20"));
        $livre->setPrix(5.00);
        $livre->setStock(200);
        $manager->persist($livre);

        $manager->flush();
    }
}
