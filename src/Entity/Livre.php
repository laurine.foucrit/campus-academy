<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LivreRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LivreRepository::class)
 * @ApiResource()
 */
class Livre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;
	
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Auteur", inversedBy="livres")
	 * @ORM\JoinColumn(name="auteur", referencedColumnName="id", nullable=false)
	 */
    private $auteur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_publication;
	
	/**
	 * @ORM\Column(type="float")
	 */
	private $prix;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	private $stock;
	
	public function getId(): int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        return $this;
    }

    public function getAuteur(): Auteur
    {
        return $this->auteur;
    }

    public function setAuteur(Auteur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDatePublication(): DateTime
    {
        return $this->date_publication;
    }

    public function setDatePublication(DateTime $date_publication): self
    {
        $this->date_publication = $date_publication;

        return $this;
    }
	
	public function getPrix()
	{
		return $this->prix;
	}
	
	public function setPrix($prix): void
	{
		$this->prix = $prix;
	}
	
	public function getStock()
	{
		return $this->stock;
	}
	
	public function setStock($stock): void
	{
		$this->stock = $stock;
	}
}
