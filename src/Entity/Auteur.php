<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\AuteurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AuteurRepository::class)
 */
class Auteur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;
	
	/**
	 * @var Livre[]|ArrayCollection
	 * @ORM\OneToMany(targetEntity="App\Entity\Livre", mappedBy="auteur", cascade={"persist"})
	 * @ApiSubresource()
	 */
	private $livres;
	
	public function __construct()
	{
		$this->livres = new ArrayCollection();
	}
	
	public function getId(): int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }
	
	/**
	 * @return Livre[]|ArrayCollection
	 */
	public function getLivres()
	{
		return $this->livres;
	}
	
	public function addLivre(Livre $livre){
		$this->livres->add($livre);
	}
	
	public function removeLivre(Livre $livre){
		$this->livres->removeElement($livre);
	}
}
